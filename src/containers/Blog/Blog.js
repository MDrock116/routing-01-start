import React, { Component } from 'react';
//import NewPost from './NewPost/NewPost';
import './Blog.css';
import Posts from "./Posts/Posts";
import {Redirect, Route} from "react-router-dom";
import {Link, NavLink, Switch} from "react-router-dom";
import asyncComponent from "../../hoc/asyncComponent";

const AsyncNewPost = asyncComponent(() => {
    return import('./NewPost/NewPost');
});

class Blog extends Component {
    state = {
        auth: true
    }

    render () {
        return (
            <div className="Blog">
                <header>
                    <nav>
                        <ul>
                            <li><NavLink to="/posts/"
                                         exact
                                         activeClassName="my-active" /*Option of overriding default ("active") name.  This does nothing as no class my-active exists*/
                                         activeStyle={{  /*inline style instead for this link*/
                                             color: '#fa923f',
                                             textDecoration: 'underline'
                                         }}
                            >Posts</NavLink></li> {/*exact tells CSS it's only class='active' if exact path*/}
                            <li><NavLink to={{
                                pathname: "/new-post",
                                hash: '#submit',    //sample get parameter.  doesn't work in this app
                                search: '?quick-submit=true' //sample get parameter.  doesn't work in this app
                            }}>New Post</NavLink></li>
                        </ul>
                    </nav>
                </header>
                {/*<Route exact path='/' render={()=> <h1>Home</h1>} />*/}
                {/*<Route path='/' render={()=> <h1>Home 2</h1>} />*/}
                <Switch>
                    {this.state.auth ? <Route path="/new-post" component={AsyncNewPost} /> : null}
                    <Route path="/posts" component={Posts} />
                    <Route render={()=> <h1>Not Found</h1>} /> {/*since this Route has no path var any request w/o a mathing route will be caught here*/}
                    {/*<Redirect from="/" to="/posts" /> */} {/*Redirect preferred method:*/}
                    {/*<Route path="/" component={Posts} />*/} {/*Redirects not-preferred method: doesn't ensure user is navigated to routes he should be on*/}
                </Switch>
            </div>
        );
    }
}

export default Blog;